import os
import json
from datetime import datetime
from urllib.parse import urlparse

from satnogs_api_client import fetch_observation_data_from_id, \
                               fetch_ground_station_data


def download_and_cache_data(norad_id, start, end, base_dir, prod=True):
    if not os.path.exists(base_dir):
        print('ERROR: Base directory doesn\'t exist, {}'.format(base_dir))
        return
        
    filenames = {'observations': os.path.join(base_dir, 'observations.json'),
                 'ground_stations': os.path.join(base_dir, 'ground_stations.json')}

    if all(os.path.isfile(filename) for filename in filenames.values()):
        # Load cached data from local filesystem
        with open(filenames['observations'], 'r') as f:
            observations_all = json.load(f)
        with open(filenames['ground_stations'], 'r') as f:
            ground_stations = json.load(f)
        
    else:        
        # Download the data
        observations_all = fetch_observation_data_from_id(norad_id, start=start, end=end, prod=prod)

        ground_station_ids = set(observation['ground_station'] for observation in observations_all)
        ground_stations = fetch_ground_station_data(ground_station_ids=ground_station_ids, prod=prod)

        # Cache the data in local filesystem
        with open(filenames['observations'], 'w') as outfile:
            json.dump(observations_all, outfile)

        with open(filenames['ground_stations'], 'w') as outfile:
            json.dump(ground_stations, outfile)

    return observations_all, ground_stations


def parse_start_end_time(observation):
    start = datetime.strptime(observation['start'], '%Y-%m-%dT%H:%M:%SZ')
    end = datetime.strptime(observation['end'], '%Y-%m-%dT%H:%M:%SZ')
    return start, end


def timestamp_from_filename(filename):
    # Parse the filename in the demoddata url and
    # return the timestamp.
    #
    # NOTE: Currently for SSTV images only.

    url = urlparse(filename)
    t_str = url.path.split('/')[-1].split('_')[-1].split('.')[0][:19]
    try:
        return datetime.strptime(t_str, '%Y-%m-%dT%H-%M-%S')
    except ValueError:
        pass


# Source: https://stackoverflow.com/a/34797890
def merge_date_ranges(data):
    '''
    Merge overlapping datetime ranges into non-overlapping, continuous datetime ranges.
    '''
    data = sorted(data, key=lambda d: d[0])
    
    result = []
    t_old = data[0]
    for t in data[1:]:
        if t_old[1] >= t[0]:  #I assume that the data is sorted already
            t_old = ((min(t_old[0], t[0]), max(t_old[1], t[1])))
        else:
            result.append(t_old)
            t_old = t
    else:
        result.append(t_old)
    return result
